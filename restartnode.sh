

# Get IP from leader node
leader_ip=$(docker-machine ip node1)

# Init Docker Swarm mode
echo "### Initializing Swarm mode ..."
eval $(docker-machine env node1)
docker swarm init --advertise-addr $leader_ip 
docker node update node1 --label-add master=true
# Swarm tokens
manager_token=$(docker swarm join-token manager -q)
worker_token=$(docker swarm join-token worker -q)



# Join worker nodes
echo "### Joining worker modes ..."
for c in {6..7} ; do
    eval $(docker-machine env node$c)
    docker swarm join --token $worker_token $leader_ip:2377
      if [ $c -lt 6 ];then
            eval $(docker-machine env node1)
            docker node update node$c --label-add public=false
      else 
            eval $(docker-machine env node1)
            docker node update node$c --label-add public=true
      fi
done


eval $(docker-machine env node1)




## registry
for c in {6..7} ; do
   docker-machine scp ./keys/registry.crt node$c:/home/docker/ && \
   docker-machine scp ./keys/registry.key node$c:/home/docker/
   docker-machine ssh node$c sudo mkdir -p /etc/docker/certs.d/myregistry.smartsoftgt.com:5000 && \
   docker-machine ssh node$c sudo cp /home/docker/registry.crt /etc/docker/certs.d/myregistry.smartsoftgt.com:5000/ca.crt


      docker-machine scp node$c:/etc/hosts /home/jb/Proyectos/Erp/scripts/swarm/
      if [ $c -lt 6 ];then
            sh -c "echo '$(docker-machine ip node1) myregistry.smartsoftgt.com' >> ./hosts"
      else 
            sh -c "echo '$(docker-machine ip node1) myregistry.smartsoftgt.com' >> ./hosts"
            sh -c "echo '$(docker-machine ip node$c) portrainer.smartsoftgt.com' >> ./hosts"
      fi
      docker-machine scp ./hosts node$c:/home/docker
      docker-machine ssh node$c sudo cp -f /home/docker/hosts /etc/
done
