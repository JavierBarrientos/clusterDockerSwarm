#!/bin/bash
## docker swarm init --force-new-cluster
# Creating 7 nodes 

## Crear Maquinas
echo "### Creating nodes ..."
for c in {1..7} ; do
   if [ $c -lt 2 ];then
          docker-machine create -d virtualbox --virtualbox-memory "2048" --virtualbox-cpu-count "2" node$c
    elif [ $c -lt 6 ];then
          docker-machine create -d virtualbox --engine-label public=false node$c
    else 
          docker-machine create -d virtualbox  --virtualbox-memory "2048"  --engine-label public=true  node$c
    fi
done


## Optener ip master

# Get IP from leader node
leader_ip=$(docker-machine ip node1)

# Init Docker Swarm mode
echo "### Initializing Swarm mode ..."

## cambio de entorno
eval $(docker-machine env node1)
docker swarm init --advertise-addr $leader_ip 
docker node update node1 --label-add master=true
# Swarm tokens

## Optener tokens de union


manager_token=$(docker swarm join-token manager -q)
worker_token=$(docker swarm join-token worker -q)


### formaas del token 

# Joinig manager nodes
echo "### Joining manager modes ..."
for c in {2..3} ; do
    eval $(docker-machine env node$c)
    docker swarm join --token $manager_token $leader_ip:2377
done

# Join worker nodes
echo "### Joining worker modes ..."
for c in {4..7} ; do
    eval $(docker-machine env node$c)
    docker swarm join --token $worker_token $leader_ip:2377
done


#for c in {1..7} ; do
#    docker-machine node$c ssh sudo swapoff -a  
#done


# Clean Docker client environment
echo "### Cleaning Docker client environment ..."
eval $(docker-machine env -u)
