echo "### Conectando al Nodo master (node1) ##"
echo "### Enable cluester network ##"

eval $(docker-machine env node1)
## user jbarrientos
## password 12341234

## jbarrientos:$$apr1$$j9C9HKjI$$r/Rws6WZBohaX4o/iZQFJ1
docker network create --driver  overlay --attachable=true  proxy
docker stack deploy --compose-file=./portrainer/portainer-agent-stack.yml portainer




docker service create \
    --name traefik \
    --constraint=node.role==manager \
    --publish 80:80  \
    --mount type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock \
    --network proxy \
    --label traefik.frontend.auth.basic='jbarrientos:$apr1$199SKAUt$7APJemyfsZE2./xnXWDlo.'\
    --label traefik.enable=true \
    --label traefik.frontend.rule="Host:traefik.smartsoftgt.com"\
    --label traefik.docker.network="proxy"\
    --label traefik.port=8080\
    traefik \
    --docker \
    --docker.swarmmode \
    --docker.domain=smartsoftgt.com \
    --docker.watch \
    --web


for c in {1..7} ; do
    docker-machine scp ./keys/registry.crt node$c:/home/docker/ && \
    docker-machine scp ./keys/registry.key node$c:/home/docker/
    docker-machine ssh node$c sudo mkdir -p /etc/docker/certs.d/myregistry.smartsoftgt.com:5000 && \
    docker-machine ssh node$c sudo cp /home/docker/registry.crt /etc/docker/certs.d/myregistry.smartsoftgt.com:5000/ca.crt
    docker-machine scp node$c:/etc/hosts /home/jb/Repos/DockerSwarmSO2/
    sh -c "echo '$(docker-machine ip node1) myregistry.smartsoftgt.com' >> ./hosts"
    sh -c "echo '$(docker-machine ip node$c) portrainer.smartsoftgt.com' >> ./hosts"
    docker-machine scp ./hosts node$c:/home/docker
    docker-machine ssh node$c sudo cp -f /home/docker/hosts /etc/
done


# registry
docker stack deploy --compose-file=./registry/registry-stack.yml registry


docker build -t myregistry.smartsoftgt.com:5000/agent-portainer:latest -f ./portrainer/agent-dockerfile .
docker push myregistry.smartsoftgt.com:5000/agent-portainer:latest
docker build -t myregistry.smartsoftgt.com:5000/layaut-portainer:latest -f ./portrainer/layaut-dockerfile .

docker push myregistry.smartsoftgt.com:5000/layaut-portainer:latest


docker stack deploy --compose-file=./portrainer/portainer-agent-stack.yml portainer


docker build -t myregistry.smartsoftgt.com:5000/mygrafana:latest -f ./stigm/dockerfile-grafana .

docker push myregistry.smartsoftgt.com:5000/mygrafana:latest


docker build -t myregistry.smartsoftgt.com:5000/myinfluxdb:latest -f ./stigm/dockerfile-influxdb .

docker push myregistry.smartsoftgt.com:5000/myinfluxdb:latest


docker build -t myregistry.smartsoftgt.com:5000/mytelegraf:latest -f ./stigm/dockerfile-telegraf .

docker push myregistry.smartsoftgt.com:5000/mytelegraf:latest




docker stack deploy --compose-file=./stigm/docker-compose.yml grafana






docker build -t myregistry.smartsoftgt.com:5000/mymariadb:latest -f ./prestashop/dockerfile-mariadb .

docker push myregistry.smartsoftgt.com:5000/mymariadb:latest


    docker build -t myregistry.smartsoftgt.com:5000/myprestashop:latest -f ./prestashop/dockerfile-prestashop .

docker push myregistry.smartsoftgt.com:5000/myprestashop:latest