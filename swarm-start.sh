#!/bin/bash

# Start nodes
echo "### Starting nodes ..."
for c in {1..7} ; do
    docker-machine start node$c
done

#for c in {1..7} ; do
#    docker-machine node$c ssh sudo swapoff -a  
#done


# Clean Docker client environment
echo "### Cleaning Docker client environment ..."
eval $(docker-machine env -u)